class RoomExample {
    constructor(roomClassName, toolboxClassName, toolsClassName){
        this.room = $(`${roomClassName}`); // => room
        this.toolbox = $(`${toolboxClassName}`);
        this.tools = $(`${toolsClassName}`);
    }

    addToolintoRoom(tool,roominto) {
        roominto.tools.append(tool);
    }

    // tool is DOM element
    addTooltoToolbox(tool) {
        this.toolbox.append(tool);
    }


    checkToolintoToobox(){
        const listTool = this.tools;
        let delRoom = [];
        for(var i=0; i < listTool.length; i++){
            const tool = listTool[i];
            
            const input = $(tool).find('input')[0];

            if (input.checked == true){
                this.addTooltoToolbox(tool);
                delRoom.push(i);
            }
        }
        if(delRoom.length){
            for(let i = 0; i< delRoom.length; i++){
                this.tools.splice(delRoom[i],1);
            }
        }
    }
    checkToolintoRoom(roominto){
        const listTool = this.tools;
        let delRoom = [];
        for(var i=0; i < listTool.length; i++){
            const tool = listTool[i];
            
            const input = $(tool).find('input')[0];

            if (input.checked == true){
                roominto.tools.append(tool);
                this.addToolintoRoom(tool,roominto)
                delRoom.push(i);
            }
        }
        if(delRoom.length){
            for(let i = 0; i< delRoom.length; i++){
                this.tools.splice(delRoom[i],1);
            }
        }
    }

}

window.addEventListener('load', function() {
    room = new RoomExample('.room1','.toolbox1', '.room1 .box div');
   
    $(".btnTooladdToolbox").click(function(){
        room.checkToolintoToobox();
    })

    let room2= new RoomExample('.room2', '.toolbox2', '.room2 .box');
    $(".btnTooladdRoom").click(function(){
        room.checkToolintoRoom(room2);
    })

   
})